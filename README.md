# Dots

A dotfiles manager script with inspiration from gnu-stow.
It's a package based solution that lets you be a little bit more selective of what dotfiles you want on diffrent computers without having to have multiple repos with almost the same content.

## Install

### With curl

```
curl -sSLo dots https://gitlab.com/kempe/dots/raw/master/dots
chmod +x dots
mv dots /usr/local/bin
```

### With git

```
git clone http://gitlab.titandreams.se/kempe/dots.git
cd dots
chmod +x dots
mv dots /usr/local/bin
```

## Examples
https://gitlab.com/dots-config/dots-example
